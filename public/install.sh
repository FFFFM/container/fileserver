#!/bin/bash

# Installation script
# wget -qO - https://files.ffffm.net/install.sh | bash

# Exit on error
set -e
set -o pipefail

echo
echo 'Updating packages'
apt update

echo
echo 'Upgrading packages'
apt -y full-upgrade

echo
echo 'Configuring salt'
mkdir -p /etc/salt/minion.d/
echo 'ipv6: True' > /etc/salt/minion.d/ipv6.conf
echo 'master: salt.ffm.freifunk.net' > /etc/salt/minion.d/master.conf

echo
echo 'Installing salt minion'
apt install -y salt-minion

echo
echo 'Waiting for startup'
sleep 5

echo 'Salt minion key:'
salt-call --local key.finger

echo
echo 'Please run the following commands on salt master:'
echo 'salt-key -F'
echo "salt-key -a $(hostname -A)"

