FROM nginx:alpine

#EXPOSE 80

RUN sed -i 's~/ {~/ { autoindex on;~' /etc/nginx/conf.d/default.conf

RUN rm /usr/share/nginx/html/index.html
COPY public/ /usr/share/nginx/html/
